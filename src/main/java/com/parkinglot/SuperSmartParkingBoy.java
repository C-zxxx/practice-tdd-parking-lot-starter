package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;

public class SuperSmartParkingBoy extends Boy {

    private final SmartParkingBoy smartParkingBoy;

    public SuperSmartParkingBoy(ParkingLot... parkingLot) {
        super();
        this.smartParkingBoy = new SmartParkingBoy(parkingLot);
    }

    @Override
    public ParkingTicket park(Car car) {
        ParkingLot availableParkingLot =
                Arrays.stream(this.smartParkingBoy.getParkingBoy().getParkingLots())
                        .max(Comparator.comparingDouble(parkingLot -> (double) (parkingLot.getAvailableSpace() / parkingLot.getTotalCapacity())))
                        .orElse(this.smartParkingBoy.getParkingBoy().getParkingLots()[0]);
        return availableParkingLot.park(car);
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        return this.smartParkingBoy.fetch(ticket);
    }
}
