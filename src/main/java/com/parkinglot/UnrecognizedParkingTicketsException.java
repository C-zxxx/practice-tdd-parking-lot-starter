package com.parkinglot;

public class UnrecognizedParkingTicketsException extends RuntimeException{
    public UnrecognizedParkingTicketsException() {
        super("Unrecognized parking tickets.");
    }
}
