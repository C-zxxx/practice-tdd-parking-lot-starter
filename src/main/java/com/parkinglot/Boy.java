package com.parkinglot;

public abstract class Boy {
    public abstract ParkingTicket park(Car car);

    public abstract Car fetch(ParkingTicket ticket);
}
