package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;

public class SmartParkingBoy extends Boy{

    private final ParkingBoy parkingBoy;

    public SmartParkingBoy(ParkingLot... parkingLot) {
        super();
        this.parkingBoy = new ParkingBoy(parkingLot);
    }

    @Override
    public ParkingTicket park(Car car) {
        ParkingLot availableParkingLot =
                Arrays.stream(this.parkingBoy.getParkingLots())
                        .max(Comparator.comparing(ParkingLot::getAvailableSpace))
                        .orElse(this.parkingBoy.getParkingLots()[0]);
        return availableParkingLot.park(car);
    }

    @Override
    public Car fetch(ParkingTicket parkingTicket) {
        return this.parkingBoy.fetch(parkingTicket);
    }

    public ParkingBoy getParkingBoy() {
        return parkingBoy;
    }

}
