package com.parkinglot;

public class NoAvailablePosition extends RuntimeException {
    public NoAvailablePosition() {
        super("No Available Position.");
    }
}
