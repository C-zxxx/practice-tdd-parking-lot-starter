package com.parkinglot;

import java.util.Arrays;

public class ParkingBoy extends Boy{

    private final ParkingLot[] parkingLots;

    public ParkingBoy(ParkingLot... parkingLot) {
        super();
        this.parkingLots = parkingLot;
    }

    @Override
    public ParkingTicket park(Car car) {
        ParkingLot availableParkingLot = Arrays.stream(parkingLots).filter(parkingLot -> parkingLot.getAvailableSpace() > 0).findFirst().orElse(parkingLots[0]);
        return availableParkingLot.park(car);

    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        ParkingLot rightParkingLot = Arrays.stream(parkingLots).filter(parkingLot -> parkingLot.getTickets().contains(ticket)).findFirst().orElse(parkingLots[0]);
        return rightParkingLot.fetch(ticket);

    }

    public ParkingLot[] getParkingLots() {
        return parkingLots;
    }
}
