package com.parkinglot;

import java.util.ArrayList;
import java.util.HashMap;

public class ParkingLot {
    private ArrayList<ParkingTicket> tickets;
    private int availableSpace;
    private final int DefaultAvailable = 10;
    private final int totalCapacity;

    public ParkingLot() {
        this.tickets = new ArrayList<>();
        this.availableSpace = DefaultAvailable;
        this.totalCapacity = DefaultAvailable;
    }

    public ParkingLot(int availableSpace) {
        this.tickets = new ArrayList<>();
        this.availableSpace = availableSpace;
        this.totalCapacity = availableSpace;
    }

    public ParkingTicket park(Car car) {
        if (this.getAvailableSpace() > 0) {
            ParkingTicket ticket = new ParkingTicket(car);
            tickets.add(ticket);
            this.setAvailableSpace(this.getAvailableSpace() - 1);
            return ticket;
        } else {
            throw new NoAvailablePosition();
        }
    }

    public Car fetch(ParkingTicket ticket) {
        if (tickets.contains(ticket)) {
            Car car = ticket.getCar();
            if (car != null) {
                this.setAvailableSpace(this.getAvailableSpace() + 1);
                return car;
            }
        }
        throw new UnrecognizedParkingTicketsException();
    }

    public int getAvailableSpace() {
        return availableSpace;
    }

    public void setAvailableSpace(int availableSpace) {
        this.availableSpace = availableSpace;
    }

    public ArrayList<ParkingTicket> getTickets() {
        return tickets;
    }

    public int getTotalCapacity() {
        return totalCapacity;
    }
}
