package com.parkinglot;

import java.util.ArrayList;

public class ParkingLotManager {

    private ArrayList<Boy> boys;

    //    实现manager自己的停车功能
    private final ParkingBoy parkingBoy;


    public ParkingLotManager() {
        this.boys = new ArrayList<>();
        this.parkingBoy = new ParkingBoy(new ParkingLot());
    }

    public ParkingLotManager(ParkingLot... parkingLots) {
        this.boys = new ArrayList<>();
        this.parkingBoy = new ParkingBoy(parkingLots);
    }

    public void addParkingBoyToList(Boy boy) {
        this.boys.add(boy);
    }
    //    指定马仔停车
    public ParkingTicket park(Boy boy, Car car) {
        if (isMyParkingBoy(boy)) {
            return boy.park(car);
        }
        System.out.println("The parking boy is not belong to this manager.");
        return null;
    }

    //    自己停车
    public ParkingTicket park(Car car) {
        return this.parkingBoy.park(car);
    }


    //     指定马仔取车
    public Car fetch(Boy boy, ParkingTicket ticket) {
        if (isMyParkingBoy(boy)) {
            return boy.fetch(ticket);
        }
        System.out.println("The parking boy is not belong to this manager.");
        return null;
    }

    //     自己取车
    public Car fetch(ParkingTicket ticket) {
        return this.parkingBoy.fetch(ticket);
    }


    //    判断是否是自己的马仔
    public boolean isMyParkingBoy(Boy boy) {
        return this.boys.contains(boy);
    }

    public ArrayList<Boy> getBoys() {
        return this.boys;
    }


}
