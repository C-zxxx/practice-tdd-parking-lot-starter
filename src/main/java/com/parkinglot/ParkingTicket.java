package com.parkinglot;

public class ParkingTicket {

    private final Car car;
    private boolean isUsed;

    public ParkingTicket(Car car) {
        this.car = car;
        this.isUsed = false;
    }

    public Car getCar() {
        Car fetchCar = this.getIsUsed() ? null : car;
        this.setIsUsed(true);
        return fetchCar;
    }

    public boolean getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(boolean isUsedd) {
        this.isUsed = isUsedd;
    }
}
