package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {
    @Test
    void should_return_ticket_when_park_given_parking_boy_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();

        //when
        ParkingTicket ticket = parkingBoy.park(car);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_boy_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        ParkingTicket ticket = parkingBoy.park(car);

        //when
        Car fetchCar = parkingBoy.fetch(ticket);

        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_return_right_car_when_fetch_given_parking_boy_with_lots_car_and_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket ticket1 = parkingBoy.park(car1);
        ParkingTicket ticket2 = parkingBoy.park(car2);

        //when
        Car fetchCar1 = parkingBoy.fetch(ticket1);
        Car fetchCar2 = parkingBoy.fetch(ticket2);

        //then
        assertEquals(car1, fetchCar1);
        assertEquals(car2, fetchCar2);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_boy_with_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        Car car = new Car();
        ParkingTicket wrongTicket = new ParkingTicket(new Car());

        //when
        var exception = assertThrows(UnrecognizedParkingTicketsException.class, () -> parkingBoy.fetch(wrongTicket));

        //then
        assertEquals("Unrecognized parking tickets.", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_boy_with_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        Car car = new Car();
        ParkingTicket ticket = parkingBoy.park(car);

        //when
        Car fetchCar1 = parkingBoy.fetch(ticket);
        var exception = assertThrows(UnrecognizedParkingTicketsException.class, () -> parkingBoy.fetch(ticket));

        //then
        assertEquals(car, fetchCar1);
        assertEquals("Unrecognized parking tickets.", exception.getMessage());

    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_parking_boy_is_full_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        parkingBoy.park(new Car());

        //when
        var exception = assertThrows(NoAvailablePosition.class, () -> parkingBoy.fetch(parkingBoy.park(new Car())));

        //then
        assertEquals("No Available Position.", exception.getMessage());
    }

    @Test
    void should_return_ticket_when_park_given_parking_boy_and_car_and_two_parking_lots() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();

        //when
        ParkingTicket ticket = parkingBoy.park(car);

        //then
        assertNotNull(ticket);
        assertTrue(parkingLot1.getTickets().contains(ticket));
    }

    @Test
    void should_return_ticket_in_second_lot_when_park_given_parking_boy_and_car_and_two_parking_lots_and_first_full_second_available() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);

        //when
        parkingBoy.park(new Car());
        ParkingTicket ticket = parkingBoy.park(new Car());

        //then
        assertNotNull(ticket);
        assertTrue(parkingLot2.getTickets().contains(ticket));
    }

    @Test
    void should_return_right_cars_when_park_given_parking_boy_and_car_and_two_parking_lots_and_two_car_in_different_lot() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = parkingBoy.park(car1);
        ParkingTicket parkingTicket2 = parkingBoy.park(car2);

        //when
        Car fetchCar1 = parkingBoy.fetch(parkingTicket1);
        Car fetchCar2 = parkingBoy.fetch(parkingTicket2);

        //then
        assertEquals(car1, fetchCar1);
        assertEquals(car2, fetchCar2);
    }

    @Test
    void should_throw_error_message_when_park_given_parking_boy_and_car_and_two_parking_lot_and_wrong_ticket() {
        //given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        ParkingTicket wrongTicket = new ParkingTicket(new Car());

        //when
        var exception = assertThrows(UnrecognizedParkingTicketsException.class, () -> parkingBoy.fetch(wrongTicket));

        //then
        assertEquals("Unrecognized parking tickets.", exception.getMessage());

    }

    @Test
    void should_throw_error_message_when_park_given_parking_boy_and_car_and_two_parking_lot_and_used_ticket() {
        //given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(),new ParkingLot());
        ParkingTicket parkingTicket = parkingBoy.park(new Car());
        parkingBoy.fetch(parkingTicket);

        //when
        var exception = assertThrows(UnrecognizedParkingTicketsException.class, () -> parkingBoy.fetch(parkingTicket));

        //then
        assertEquals("Unrecognized parking tickets.", exception.getMessage());

    }

    @Test
    void should_throw_error_message_when_park_given_parking_boy_and_car_and_two_full_parking_lot() {
        //given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(1), new ParkingLot(1));
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());

        //when
        var exception = assertThrows(NoAvailablePosition.class, () -> parkingBoy.park(new Car()));

        //then
        assertEquals("No Available Position.", exception.getMessage());

    }


}
