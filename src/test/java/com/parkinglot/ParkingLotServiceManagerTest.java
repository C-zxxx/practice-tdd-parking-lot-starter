package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotServiceManagerTest {
    @Test
    void should_return_parkingboy_list_when_addParkingBoy_given_parkingboys() {
        //given
        ParkingBoy parkingBoy1 = new ParkingBoy(new ParkingLot());
        SuperSmartParkingBoy parkingBoy2 = new SuperSmartParkingBoy(new ParkingLot());
        ParkingLotManager parkingLotManager = new ParkingLotManager();

        //when
        parkingLotManager.addParkingBoyToList(parkingBoy1);
        parkingLotManager.addParkingBoyToList(parkingBoy2);

        //then
        assertTrue(parkingLotManager.getBoys().contains(parkingBoy1));
        assertTrue(parkingLotManager.getBoys().contains(parkingBoy2));
    }

    @Test
    void should_return_ticket_when_park_given_parkingLotManager_and_specify_parkingBoy_and_car() {
        //given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        Car car = new Car();

        //when
        parkingLotManager.addParkingBoyToList(parkingBoy);
        ParkingTicket ticket = parkingLotManager.park(parkingBoy, car);

        //then
        assertNotNull(ticket);

    }

    @Test
    void should_return_car_when_fetch_given_parkingLotManager_and_specify_parkingBoy_and_ticket() {
        //given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        Car car = new Car();
        parkingLotManager.addParkingBoyToList(parkingBoy);
        ParkingTicket ticket = parkingLotManager.park(parkingBoy, car);

        //when
        Car fetchCar = parkingLotManager.fetch(parkingBoy, ticket);

        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_return_ticket_when_park_given_parkingLotManager_and_and_car_by_himself() {
        //given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        Car car = new Car();

        //when
        ParkingTicket ticket = parkingLotManager.park(car);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parkingLotManager_and_ticket_by_himself() {
        //given
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        Car car = new Car();
        ParkingTicket ticket = parkingLotManager.park(car);

        //when
        Car fetchCar = parkingLotManager.fetch(ticket);

        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_throw_error_when_park_given_parkingLotManager_and_full_parkingLot_and_parkingBoy_and_cars() {
        //given
        ParkingBoy parkingBoy1 = new ParkingBoy(new ParkingLot(1));
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        parkingLotManager.addParkingBoyToList(parkingBoy1);
        parkingLotManager.park(parkingBoy1, new Car());

        //when
        var exception = assertThrows(NoAvailablePosition.class, () -> parkingLotManager.park(parkingBoy1, new Car()));

        //then
        assertEquals("No Available Position.", exception.getMessage());
    }

    @Test
    void should_throw_error_when_fetch_given_parkingLotManager_and_parkingLot_and_parkingBoy_and_wrong_ticket() {
        //given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        parkingLotManager.addParkingBoyToList(parkingBoy);

        //when
        var exception = assertThrows(UnrecognizedParkingTicketsException.class, () -> parkingLotManager.fetch(parkingBoy, new ParkingTicket(new Car())));

        //then
        assertEquals("Unrecognized parking tickets.", exception.getMessage());
    }

    @Test
    void should_throw_error_when_fetch_given_parkingLotManager_and_parkingLot_and_parkingBoy_and_used_ticket() {
        //given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        parkingLotManager.addParkingBoyToList(parkingBoy);
        ParkingTicket ticket = parkingLotManager.park(parkingBoy, new Car());
        parkingLotManager.fetch(parkingBoy, ticket);

        //when
        var exception = assertThrows(UnrecognizedParkingTicketsException.class, () -> parkingLotManager.fetch(parkingBoy, ticket));

        //then
        assertEquals("Unrecognized parking tickets.", exception.getMessage());
    }


}
