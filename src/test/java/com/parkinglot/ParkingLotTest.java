package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {

    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        //when
        ParkingTicket ticket = parkingLot.park(car);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket ticket = parkingLot.park(car);

        //when
        Car fetchCar = parkingLot.fetch(ticket);

        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_return_right_car_when_fetch_given_parking_lot_with_lots_car_and_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket ticket1 = parkingLot.park(car1);
        ParkingTicket ticket2 = parkingLot.park(car2);

        //when
        Car fetchCar1 = parkingLot.fetch(ticket1);
        Car fetchCar2 = parkingLot.fetch(ticket2);

        //then
        assertEquals(car1, fetchCar1);
        assertEquals(car2, fetchCar2);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_with_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket wrongTicket = new ParkingTicket(new Car());

        //when
        var exception = assertThrows(UnrecognizedParkingTicketsException.class, () -> parkingLot.fetch(wrongTicket));

        //then
        assertEquals("Unrecognized parking tickets.", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_with_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket ticket = parkingLot.park(car);

        //when
        Car fetchCar1 = parkingLot.fetch(ticket);
        var exception = assertThrows(UnrecognizedParkingTicketsException.class, () -> parkingLot.fetch(ticket));

        //then
        assertEquals(car, fetchCar1);
        assertEquals("Unrecognized parking tickets.", exception.getMessage());

    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_parking_lot_is_full_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        parkingLot.park(new Car());

        //when
        var exception = assertThrows(NoAvailablePosition.class, () -> parkingLot.fetch(parkingLot.park(new Car())));

        //then
        assertEquals("No Available Position.", exception.getMessage());
    }


}
