package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SuperSmartParkingBoyTest {
    @Test
    void should_park_cars_to_larger_available_position_rate_when_park_given_ssp_boy_and_cars_and_two_parking_lots() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(3);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);

        //when
        superSmartParkingBoy.park(new Car());
        superSmartParkingBoy.park(new Car());

        //then
        assertEquals(2, parkingLot1.getAvailableSpace());
        assertEquals(0, parkingLot2.getAvailableSpace());
    }
}
