package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class SmartParkingBoyTest {
    @Test
    void should_park_cars_to_the_parking_lot_which_contains_more_empty_positions_when_park_given_parking_boy_and_car_and_two_parking_lots() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(3);
        ParkingLot parkingLot2 = new ParkingLot(3);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        ArrayList<ParkingTicket> tickets = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            tickets.add(smartParkingBoy.park(new Car()));
        }
        smartParkingBoy.fetch(tickets.get(2));

        //when
        smartParkingBoy.park(new Car());

        //then
        assertEquals(1, parkingLot1.getAvailableSpace());
        assertEquals(1, parkingLot2.getAvailableSpace());
    }


}
